<!DOCTYPE html>
<html>
<head>
    <?php 
    $this_style = "./css/profile.css";
    require './blocks/head.php'; ?>
</head>

<body>
    <?php require './blocks/searching.php'; ?>

      <section>
        <div class="pages">
            <div class="container">
                <div class="pages__inner">
                    <div class="pages__inner-developer">
                        <img src="./img/me.jpg" alt="Maxym Kachmaryk">
                    </div>
                    <div class="pages__inner-information">
                        <ul class="list">
                            <li>Розробив сайт студент групи КН-307</li>
                            <li class="name">Качмарик Максим</li>
                        </ul>
                    </div>
                    <?php if ($_COOKIE['user']!=''): ?>
                    <button class="btn btn-danger" id="exit_btn">Вийти</button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
  
    
    <?php require './blocks/footer.php'; ?>

    <!-- jQuery first, then Popper.js, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   
   <script>
           $('#exit_btn').click(function() {
           $.ajax({
               url: '../reg/exit.php',
               type: 'POST',
               cache: false,
               data: {},
               dataType: 'html',
               success: function(data) {
                   document.location.replace('../signIn.php');
               }
           });
       });
 </script>

</body>
</html>